package MENDILLO;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Window;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;

public class Management {

	JFrame LMframe;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Management window = new Management();
					window.LMframe.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Management() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		LMframe = new JFrame(); //create a new JFrame
		LMframe.setBounds(100, 100, 553, 355); //set the size and location of the frame
		LMframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //set the default close operation when the user clicks the "x" button
		LMframe.getContentPane().setLayout(null); //set the layout to null (no layout manager)

		//create a new JLabel with the title text and font, and set its position and size
		JLabel titlelabel = new JLabel("LIBRARY MANAGEMENT");
		titlelabel.setHorizontalAlignment(SwingConstants.CENTER);
		titlelabel.setFont(new Font("STZhongsong", Font.BOLD, 20));
		titlelabel.setBounds(125, 27, 280, 35);
		LMframe.getContentPane().add(titlelabel); //add the label to the frame's content pane

		//create a new JButton with the admin login text and font, and set its position and size
		JButton Adminbutton = new JButton("Admin Login");
		Adminbutton.addActionListener(new ActionListener() {
				//add an action listener that creates a new AdminLogin window and disposes of the current window
				public void actionPerformed(ActionEvent e) {
					Login window = new Login();
					((Window) window.loginframe).setVisible(true);
					LMframe.dispose();
			}
		});
		Adminbutton.setFont(new Font("STZhongsong", Font.BOLD, 18));
		Adminbutton.setBounds(180, 101, 170, 42);
		LMframe.getContentPane().add(Adminbutton); //add the button to the frame's content pane

		//create a new JButton with the librarian login text and font, and set its position and size
		JButton Librarianbutton = new JButton("Librarian  Login");
		Librarianbutton.setFont(new Font("STZhongsong", Font.BOLD, 15));
		Librarianbutton.addActionListener(new ActionListener() {
			//add an action listener that creates a new LibrarianLogin window and disposes of the current window
			public void actionPerformed(ActionEvent e) {
				LibLogin window = new LibLogin();
				window.Librarianframe.setVisible(true);
				LMframe.dispose();
				
			}
		});
		Librarianbutton.setBounds(180, 159, 170, 42);
		LMframe.getContentPane().add(Librarianbutton); //add the button to the frame's content pane
	}
}
