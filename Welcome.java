package MENDILLO;



import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;

public class Welcome {

	public JFrame LWframe;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Welcome window = new Welcome();
					window.LWframe.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Welcome() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		LWframe = new JFrame();
		LWframe.setBounds(100, 100, 534, 363);
		LWframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		LWframe.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Welcome to NU Library System");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("STZhongsong", Font.BOLD, 21));
		lblNewLabel.setBounds(75, 117, 374, 54);
		LWframe.getContentPane().add(lblNewLabel);
		
		JButton btnNewButton = new JButton("Exit");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Management window = new Management();
				window.LMframe.setVisible(true);
				LWframe.dispose();
			}
		});
		btnNewButton.setFont(new Font("Times New Roman", Font.BOLD, 15));
		btnNewButton.setBounds(214, 244, 100, 36);
		LWframe.getContentPane().add(btnNewButton);
	}
}

